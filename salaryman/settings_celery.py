from celery import Celery
import os


PROJECT_NAME = 'salaryman'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{}.settings'.format(PROJECT_NAME))

from django.conf import settings  # noqa
app = Celery(PROJECT_NAME)
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

app.conf.update(
    BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='djcelery.backends.cache:CacheBackend',
    CELERY_TIMEZONE='Asia/Seoul',
    CELERY_ENABLE_UTC=False
)   

from datetime import timedelta
from celery.schedules import crontab


