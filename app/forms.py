from django import forms
from .models import (
    User,
    Document,
)

from django.contrib.auth.forms import UserCreationForm

class UserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username']

class DocumentUploadForm(forms.ModelForm):
    is_translate = forms.BooleanField(initial=False, required=False)
    class Meta:
        model = Document
        fields = ['url', 'type']
        
    def __init__(self, *args, **kwargs):
        super(DocumentUploadForm, self).__init__(*args, **kwargs)
        self.fields['type'].widget.attrs.update({
            'class': 'browser-default'
        })