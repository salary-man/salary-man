"""salaryman URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

from . import views
    
from graphene_django.views import GraphQLView

urlpatterns = [
    url(r'^test/', views.test),
    url(r'^giftcode/', views.giftcode, name='giftcode'),
    url(r'^api/v1/login', views.LoginView.as_view(), name='api_login'),
    url(r'^accounts/logout/$', auth_views.logout, {'next_page' : '/'}, name='logout'),
    url(r'^accounts/login/$', views.LoginView.as_view(), name='login'),
    url(r'^accounts/register/$', views.register, name='register'),
#    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^documents/upload/$', views.DocumentUploadView.as_view(), name='documents_upload'),
    url(r'^documents/list/$', views.DocumentListView.as_view(), name='documents_list'),
    url(r'^documents/(?P<pk>\d+)/$', views.DocumentDetailView.as_view(), name='documents_detail'),

   # url(r'^graphql/$', GraphQLView.as_view(graphiql=True)),
    url(r'^$', views.index, name='home'),
]



