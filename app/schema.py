from graphene_django import DjangoObjectType
import graphene

from django.contrib.auth.hashers import make_password

from .models import User

class UserType(DjangoObjectType):
    class Meta:
        model = User

class Query(graphene.ObjectType):
    user = graphene.Field(UserType, username=graphene.String(), password=graphene.String(), id=graphene.Int())
    def resolve_user(self, info, **args):
        id = args.get('id')
        username = args.get('username')
        password = args.get('password')
           
        if id:
            user = User.objects.get(id=id)
        else:
            password = make_password(password)
            if not (username and password):
                return None
            user = User.objects.get(username=username, password=password)
        return user

schema = graphene.Schema(query=Query)
