from __future__ import absolute_import, unicode_literals

from tempfile import NamedTemporaryFile
import socket
import urllib
import urllib2
from urlparse import urlparse
import os
import time

from bs4 import BeautifulSoup
from googletrans import Translator
from docx import Document as Docx
from celery import task

from app.models import Document

def translate(contents):
    translator = Translator()
    if isinstance(contents, list):
        contents = map(lambda x: contents[:32], contents) #truncate against maxnimum contents
    else:
        contents = contents[:4096]
    contents = translator.translate(contents, dest='ko')
    if isinstance(contents, list):
        contents = ' '.join(map(lambda x: x.text, contents))
    else:
        contents = contents.text
    return contents

def download_docx(url):
    path = ''
    try:
        path, res = urllib.urlretrieve(url)
        contents = ''
        try:
            docx = Docx(path)
            contents = map(lambda x: x.text, docx.paragraphs)
        except ValueError as e: #big file
            raise e
        except Exception as e: #temporariliy translate
            with open(path) as f:
                contents = f.read().encode('base64')
        return contents
    finally:
        global path
        if path.startswith('/var') or path.startswith('/tmp'):
            os.remove(path)

def download_page(url):
    contents = ''
    try:
        res = urllib2.urlopen(url)
        file_size = res.info().getheaders('Content-Length')
        if file_size:
            if file_size[0] > 5*1024*1024:
                raise ValueError('Big')
        contents = res.read()
        res.close()
        
        bs4 = BeautifulSoup(contents, 'html.parser')
        contents = bs4.html.body.text
        return contents

    except ValueError as e:
        raise e
    except:
        return contents

@task
def upload_document(user, url, type, is_translate):
    parsed_url = urlparse(url)
    name = os.path.basename(parsed_url.path)
    document = Document(user=user, url=url, name=name, contents='')
    document.save()
    try:
        time.sleep(3) 
        socket.setdefaulttimeout(5)
        contents = 'Error'
        if type == Document.DOCX:
            contents = download_docx(url)
        else:
            contents = download_page(url)
           
        if is_translate:
            contents = translate(contents)
        else:
            if isinstance(contents, list):
                contents = ''.join(contents)
        document.contents = contents
        document.status = 'Success'
        return True

    except ValueError as e:
        document.status = 'Failed'
        document.status_message = 'File is too big or Translation is not available'
        #import traceback
        #traceback.print_exc()
        return False
    except Exception as e:
        document.status = 'Failed'
        document.status_message = 'Unknown Error' #RELEASE
        return False
    finally:
        document.save()
        
