# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
    REQUIRED_FIELDS = []
    is_admin = models.BooleanField(default=False)
    is_premium = models.BooleanField(default=False)
    gift_code = models.CharField(default='', max_length=32)

class Document(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='documents')   
    url = models.CharField(max_length=1024)
    name = models.CharField(max_length=256, blank=True)
    contents = models.TextField(blank=True)
    DOCX = 0
    PAGE = 1
    TYPES = (
        (0, 'Docx'),
        (1, 'Page'),
    ) 

    type = models.IntegerField(default=0, choices=TYPES) 
    status = models.CharField(default='Processing in 5 secs', max_length=16)
    status_message = models.CharField(default='', max_length=4096)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}:{}:{}'.format(self.url, self.name, self.contents)