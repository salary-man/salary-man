# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from urlparse import urlparse

from googletrans import Translator

from django.core.exceptions import (
    ObjectDoesNotExist,
    ValidationError,
)

from django.shortcuts import (
    render, 
    redirect, 
    HttpResponse
)

from django.http import (
 HttpResponseBadRequest,
 JsonResponse
)

from django.contrib.auth import (
    login,
    authenticate
)

from django.contrib.auth.views import LoginView as DjangoLoginView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


from django.urls import reverse_lazy

from django.views.generic.edit import FormView
from django.views.generic import (
    ListView,
    DetailView,
)

from .tasks import  upload_document

from .forms import (
    UserCreationForm,
    DocumentUploadForm,
)

from .models import (
    User,
    Document,
)
from .schema import schema
# Create your views here.


def index(request):
    return render(request, 'index.html', context={})
    
def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('app:home')
    else:
        form = UserCreationForm()
        
    return render(request, 'registration/register.html', {'form': form})

@login_required
def giftcode(request):
    context = {}
    if request.method == 'POST':
        try:
            code = request.POST.get('code','')
            admin = User.objects.get(username='admin')
            if admin.gift_code.strip() == code.strip():
                request.user.is_premium = True
                request.user.save()
                context = {'message':'Correct! Hi Premium User! Go to Page Crawl Feature'}
            else:
                context = {'message':'Wrong! Login admin!'}
        except:
            context = {'message':'Unknown Error'}

    return render(request, 'giftcode.html', context)

class LoginView(DjangoLoginView):
    def post(self, request, *args, **kwargs):
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        if not (username and password):
            return HttpResponseBadRequest()
        #query = 'query {{ user(username:"{username}", password:"{password}") {{  id, email }} }}'.format(
        query = 'query {{ user(username:"{username}", password:"{password}") {{  id }} }}'.format(
            **{'username':username, 'password':password})

        result = schema.execute(query)
        
        errors = ['', str(result.errors)][result.errors != None]
        result = {'data': result.data, 'errors':errors}
        try:
            if 'does not exist' in result['errors']: 
                raise ObjectDoesNotExist()

            if not result['errors']: #user exists
                user = result['data']['user']
                if 'id' in user:
                    user = User.objects.get(pk=user['id'])
                    login(request, user)
                    return JsonResponse({'data':'Welcome! '+user.username, 'errors':None})
            return JsonResponse(result) #for debug

        except ObjectDoesNotExist:
            return JsonResponse({'data':None, 'errors':'Incorrect ID/PW'})
        except:
            return HttpResponseBadRequest()
            
      
class DocumentUploadView(LoginRequiredMixin, FormView):
    template_name = 'documents/upload.html'
    form_class = DocumentUploadForm 
    success_url = reverse_lazy('app:documents_list')

    def form_valid(self, form):
        type = form.cleaned_data['type']
        url = form.cleaned_data['url']
        is_translate = form.cleaned_data['is_translate']

        user = self.request.user

        if user.is_admin:
            form._errors[''] = 'Admin is not availiable'
            return self.form_invalid(form)

        result = urlparse(url)
        scheme = result.scheme
        if not scheme:
            form._errors[''] = 'Required URL scheme'
            return self.form_invalid(form)
        if scheme == 'file':
            form._errors[''] = "Nice Try, Don't SSRF ! "
            return self.form_invalid(form)
            
        if type == Document.PAGE and not user.is_premium:
            form._errors[''] = 'Page is only for PREMIUM'
            return self.form_invalid(form)

                
        upload_document.delay(user, url, type, is_translate)
        
        return super(DocumentUploadView, self).form_valid(form)

    def form_invalid(self, form):
        type = form.cleaned_data['type']
        return super(DocumentUploadView, self).form_invalid(form)


class DocumentListView(LoginRequiredMixin, ListView):
    template_name = 'documents/list.html'
    context_object_name = 'documents' 

    def get_queryset(self): 
        if self.request.user.is_authenticated:
            return self.request.user.documents.all()
        return None


class DocumentDetailView(LoginRequiredMixin, DetailView):
    template_name = 'documents/detail.html'
    model = Document
    context_object_name = 'document' 
    def get_context_data(self, **kwargs):
        try:
            document = Document.objects.get(user=self.request.user,pk=self.kwargs['pk'])
        except Exception as e:
            document = None
        return {'document':document}
            
def test(request):
    return render(request, 'add.html')